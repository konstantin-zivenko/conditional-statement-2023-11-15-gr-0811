# Написати програму, яка визначає яким є число - парним чи непарним. Якщо залишок від розподілу на 2 не
# дорівнює 0, це непарне число, а якщо дорівнює- то парне.

symbols = input("введіть число: ")
# Preparation
symbols = symbols.translate(
    str.maketrans(",", ".", " ")
)

if symbols[0] == "+":
    symbols = symbols[1:]

# Validation

if not symbols.isdigit():
    print(" число повинно включати лише цифри")

number = int(symbols)
if number % 2:
    print(f"число {number} не є парним")
else:
    print(f"число {number} є парним")
